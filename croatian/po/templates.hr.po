msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 16:57+0200\n"
"Last-Translator: Josip Rodin\n"
"Language-Team: Croatian\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/search.xml.in:7
#, fuzzy
#| msgid "Debian Project"
msgid "Debian website"
msgstr "Debian Projekt"

#: ../../english/search.xml.in:9
msgid "Search the Debian website."
msgstr ""

#: ../../english/template/debian/basic.wml:19
#: ../../english/template/debian/navbar.wml:11
#, fuzzy
#| msgid "Help Debian"
msgid "Debian"
msgstr "Pomozite Debianu"

#: ../../english/template/debian/basic.wml:46
msgid "Debian website search"
msgstr ""

#: ../../english/template/debian/common_translation.wml:4
msgid "Yes"
msgstr "Da"

#: ../../english/template/debian/common_translation.wml:7
msgid "No"
msgstr "Ne"

#: ../../english/template/debian/common_translation.wml:10
msgid "Debian Project"
msgstr "Debian Projekt"

#: ../../english/template/debian/common_translation.wml:13
#, fuzzy
#| msgid ""
#| "Debian GNU/Linux is a free distribution of the GNU/Linux operating "
#| "system. It is maintained and updated through the work of many users who "
#| "volunteer their time and effort."
msgid ""
"Debian is an operating system and a distribution of Free Software. It is "
"maintained and updated through the work of many users who volunteer their "
"time and effort."
msgstr ""
"Debian GNU/Linux je slobodna distribucija operativnog sustava GNU/Linux. "
"Održava se i osvježava radom mnogih korisnika koji dobrovoljno ulažu vrijeme "
"i napor."

#: ../../english/template/debian/common_translation.wml:16
msgid "debian, GNU, linux, unix, open source, free, DFSG"
msgstr "debian, GNU, linux, unix, open source, free, slobodan, DFSG"

#: ../../english/template/debian/common_translation.wml:19
msgid "Back to the <a href=\"m4_HOME/\">Debian Project homepage</a>."
msgstr "Natrag na <a href=\"m4_HOME/\">početnu stranicu Debian Projekta</a>."

#: ../../english/template/debian/common_translation.wml:22
#: ../../english/template/debian/links.tags.wml:149
msgid "Home"
msgstr "Početak"

#: ../../english/template/debian/common_translation.wml:25
msgid "Skip Quicknav"
msgstr "Preskoči brzu navigaciju"

#: ../../english/template/debian/common_translation.wml:28
msgid "About"
msgstr "O&nbsp;Debianu"

#: ../../english/template/debian/common_translation.wml:31
msgid "About Debian"
msgstr "O Debianu"

#: ../../english/template/debian/common_translation.wml:34
msgid "Contact Us"
msgstr "Kontaktirajte nas"

#: ../../english/template/debian/common_translation.wml:37
msgid "Donations"
msgstr "Donacije"

#: ../../english/template/debian/common_translation.wml:40
msgid "Events"
msgstr "Zbivanja"

#: ../../english/template/debian/common_translation.wml:43
msgid "News"
msgstr "Novosti"

#: ../../english/template/debian/common_translation.wml:46
msgid "Distribution"
msgstr "Distribucija"

#: ../../english/template/debian/common_translation.wml:49
msgid "Support"
msgstr "Podrška"

#: ../../english/template/debian/common_translation.wml:52
msgid "Pure Blends"
msgstr ""

#: ../../english/template/debian/common_translation.wml:55
#: ../../english/template/debian/links.tags.wml:46
msgid "Developers' Corner"
msgstr "Kutak za razvijatelje"

#: ../../english/template/debian/common_translation.wml:58
msgid "Documentation"
msgstr "Dokumentacija"

#: ../../english/template/debian/common_translation.wml:61
msgid "Security Information"
msgstr "Sigurnosne informacije"

#: ../../english/template/debian/common_translation.wml:64
msgid "Search"
msgstr "Pretraživanje"

#: ../../english/template/debian/common_translation.wml:67
msgid "none"
msgstr "nema"

#: ../../english/template/debian/common_translation.wml:70
msgid "Go"
msgstr "Kreni"

#: ../../english/template/debian/common_translation.wml:73
msgid "worldwide"
msgstr "širom svijeta"

#: ../../english/template/debian/common_translation.wml:76
msgid "Site map"
msgstr "Pregled web stranica"

#: ../../english/template/debian/common_translation.wml:79
msgid "Miscellaneous"
msgstr "Razno"

#: ../../english/template/debian/common_translation.wml:82
#: ../../english/template/debian/links.tags.wml:104
msgid "Getting Debian"
msgstr "Nabavka Debiana"

#: ../../english/template/debian/common_translation.wml:85
#, fuzzy
#| msgid "Debian Books"
msgid "The Debian Blog"
msgstr "Knjige o Debianu"

#: ../../english/template/debian/ddp.wml:6
msgid ""
"Please send all comments, criticisms and suggestions about these web pages "
"to our <a href=\"mailto:debian-doc@lists.debian.org\">mailing list</a>."
msgstr ""
"Molimo pošaljite sve komentare, kritike i sugestije o ovim web stranicama na "
"našu <a href=\"mailto:debian-doc@lists.debian.org\">mailing listu</a>."

#: ../../english/template/debian/fixes_link.wml:11
msgid "not needed"
msgstr "nije potrebno"

#: ../../english/template/debian/fixes_link.wml:14
msgid "not available"
msgstr "nije dostupno"

#: ../../english/template/debian/fixes_link.wml:17
msgid "N/A"
msgstr "nije primjenjivo"

#: ../../english/template/debian/fixes_link.wml:20
msgid "in release 1.1"
msgstr "u verziji 1.1"

#: ../../english/template/debian/fixes_link.wml:23
msgid "in release 1.3"
msgstr "u verziji 1.3"

#: ../../english/template/debian/fixes_link.wml:26
msgid "in release 2.0"
msgstr "u verziji 2.0"

#: ../../english/template/debian/fixes_link.wml:29
msgid "in release 2.1"
msgstr "u verziji 2.1"

#: ../../english/template/debian/fixes_link.wml:32
msgid "in release 2.2"
msgstr "u verziji 2.2"

#. TRANSLATORS: Please make clear in the translation of the following
#. item that mail sent to the debian-www list *must* be in English. Also,
#. you can add some information of your own translation mailing list
#. (i.e. debian-l10n-XXXXXX@lists.debian.org) for reporting things in
#. your language.
#: ../../english/template/debian/footer.wml:105
#, fuzzy
#| msgid ""
#| "To report a problem with the web site, e-mail <a href=\"mailto:debian-"
#| "www@lists.debian.org\">debian-www@lists.debian.org</a>.  For other "
#| "contact information, see the Debian <a href=\"m4_HOME/contact\">contact "
#| "page</a>."
msgid ""
"To report a problem with the web site, e-mail our publicly archived mailing "
"list <a href=\"mailto:debian-www@lists.debian.org\">debian-www@lists.debian."
"org</a>.  For other contact information, see the Debian <a href=\"m4_HOME/"
"contact\">contact page</a>. Web site source code is <a href=\"m4_HOME/devel/"
"website/using_cvs\">available</a>."
msgstr ""
"Kako bi prijavili problem s webom, pošaljite e-mail <em>na engleskom jeziku</"
"em> na adresu <a href=\"mailto:debian-www@lists.debian.org\">debian-"
"www@lists.debian.org</a>. Za ostale informacije o tome kako nas "
"kontaktirati, pogledajte Debianovu <a href=\"m4_HOME/contact\">kontaktnu "
"stranicu</a>."

#: ../../english/template/debian/footer.wml:108
msgid "Last Modified"
msgstr "Zadnji put promijenjeno"

#: ../../english/template/debian/footer.wml:111
msgid "Copyright"
msgstr "Vlasnik autorskih prava"

#: ../../english/template/debian/footer.wml:114
msgid "<a href=\"https://www.spi-inc.org/\">SPI</a> and others;"
msgstr ""

#: ../../english/template/debian/footer.wml:117
msgid "See <a href=\"m4_HOME/license\" rel=\"copyright\">license terms</a>"
msgstr ""
"Pročitajte <a href=\"m4_HOME/license\" rel=\"copyright\">uvjete licence</a>"

#: ../../english/template/debian/footer.wml:120
msgid ""
"Debian is a registered <a href=\"m4_HOME/trademark\">trademark</a> of "
"Software in the Public Interest, Inc."
msgstr ""
"Debian je registrirani <a href=\"m4_HOME/trademark\">zaštitni znak</a> u "
"vlasništvu Software in the Public Interest, Inc."

#: ../../english/template/debian/languages.wml:196
#: ../../english/template/debian/languages.wml:232
msgid "This page is also available in the following languages:"
msgstr "Ova stranica je također dostupna na sljedećim jezicima:"

#: ../../english/template/debian/languages.wml:265
msgid "How to set <a href=m4_HOME/intro/cn>the default document language</a>"
msgstr ""
"Kako postaviti <a href=m4_HOME/intro/cn>predodređeni jezik dokumenata</a>"

#: ../../english/template/debian/links.tags.wml:4
msgid "Debian International"
msgstr "Debian međunarodno"

#: ../../english/template/debian/links.tags.wml:7
msgid "Partners"
msgstr "Partneri"

#: ../../english/template/debian/links.tags.wml:10
msgid "Debian Weekly News"
msgstr "Tjedne Debian vijesti"

#: ../../english/template/debian/links.tags.wml:13
msgid "Weekly News"
msgstr "Tjedne vijesti"

#: ../../english/template/debian/links.tags.wml:16
msgid "Debian Project News"
msgstr "Debian Project News"

#: ../../english/template/debian/links.tags.wml:19
msgid "Project News"
msgstr "Project News"

#: ../../english/template/debian/links.tags.wml:22
msgid "Release Info"
msgstr "Informacije o izdanjima"

#: ../../english/template/debian/links.tags.wml:25
msgid "Debian Packages"
msgstr "Debian paketi"

#: ../../english/template/debian/links.tags.wml:28
msgid "Download"
msgstr "Prijenos Internetom"

#: ../../english/template/debian/links.tags.wml:31
msgid "Debian&nbsp;on&nbsp;CD"
msgstr "Debian&nbsp;na&nbsp;CD-u"

#: ../../english/template/debian/links.tags.wml:34
msgid "Debian Books"
msgstr "Knjige o Debianu"

#: ../../english/template/debian/links.tags.wml:37
#, fuzzy
#| msgid "Help Debian"
msgid "Debian Wiki"
msgstr "Pomozite Debianu"

#: ../../english/template/debian/links.tags.wml:40
msgid "Mailing List Archives"
msgstr "Arhive mailing lista"

#: ../../english/template/debian/links.tags.wml:43
msgid "Mailing Lists"
msgstr "Mailing liste"

#: ../../english/template/debian/links.tags.wml:49
msgid "Social Contract"
msgstr "Društveni ugovor"

#: ../../english/template/debian/links.tags.wml:52
msgid "Code of Conduct"
msgstr ""

#: ../../english/template/debian/links.tags.wml:55
msgid "Debian 5.0 - The universal operating system"
msgstr ""

#: ../../english/template/debian/links.tags.wml:58
msgid "Site map for Debian web pages"
msgstr "Pregled Debian web stranica"

#: ../../english/template/debian/links.tags.wml:61
msgid "Developer Database"
msgstr "Baza podataka o razvijateljima"

#: ../../english/template/debian/links.tags.wml:64
#, fuzzy
#| msgid "Debian Books"
msgid "Debian FAQ"
msgstr "Knjige o Debianu"

#: ../../english/template/debian/links.tags.wml:67
msgid "Debian Policy Manual"
msgstr "Debian Policy Manual"

#: ../../english/template/debian/links.tags.wml:70
msgid "Developers' Reference"
msgstr "Developers' Reference"

#: ../../english/template/debian/links.tags.wml:73
msgid "New Maintainers' Guide"
msgstr "New Maintainers' Guide"

#: ../../english/template/debian/links.tags.wml:76
msgid "Release Critical Bugs"
msgstr "Bugovi kritični za izdanje"

#: ../../english/template/debian/links.tags.wml:79
msgid "Lintian Reports"
msgstr "Lintian izvještaji"

#: ../../english/template/debian/links.tags.wml:83
msgid "Archives for users' mailing lists"
msgstr "Arhive korisničkih mailing lista"

#: ../../english/template/debian/links.tags.wml:86
msgid "Archives for developers' mailing lists"
msgstr "Arhive razvijateljskih mailing lista"

#: ../../english/template/debian/links.tags.wml:89
msgid "Archives for i18n/l10n mailing lists"
msgstr "Arhive i18n/l10n mailing lista"

#: ../../english/template/debian/links.tags.wml:92
msgid "Archives for ports' mailing lists"
msgstr "Arhive mailing lista portova"

#: ../../english/template/debian/links.tags.wml:95
msgid "Archives for mailing lists of the Bug tracking system"
msgstr "Arhive mailing lista sustava praćenja bugova"

#: ../../english/template/debian/links.tags.wml:98
msgid "Archives for miscellaneous mailing lists"
msgstr "Arhive raznih mailing lista"

#: ../../english/template/debian/links.tags.wml:101
msgid "Free Software"
msgstr "Slobodni softver"

#: ../../english/template/debian/links.tags.wml:107
msgid "Development"
msgstr "Razvoj"

#: ../../english/template/debian/links.tags.wml:110
msgid "Help Debian"
msgstr "Pomozite Debianu"

#: ../../english/template/debian/links.tags.wml:113
msgid "Bug reports"
msgstr "Bug izvještaji"

#: ../../english/template/debian/links.tags.wml:116
msgid "Ports/Architectures"
msgstr "Portovi/Arhitekture"

#: ../../english/template/debian/links.tags.wml:119
msgid "Installation manual"
msgstr "Upute za instalaciju"

#: ../../english/template/debian/links.tags.wml:122
msgid "CD vendors"
msgstr "Prodavači CD-a"

#: ../../english/template/debian/links.tags.wml:125
msgid "CD/USB ISO images"
msgstr "CD/USB ISO snimke"

#: ../../english/template/debian/links.tags.wml:128
msgid "Network install"
msgstr "Mrežna instalacija"

#: ../../english/template/debian/links.tags.wml:131
msgid "Pre-installed"
msgstr "Pred-instaliran"

#: ../../english/template/debian/links.tags.wml:134
msgid "Debian-Edu project"
msgstr "Debian-Edu projekt"

#: ../../english/template/debian/links.tags.wml:137
msgid "Alioth &ndash; Debian GForge"
msgstr "Alioth &ndash; Debian GForge"

#: ../../english/template/debian/links.tags.wml:140
msgid "Quality Assurance"
msgstr "Osiguranje kvalitete"

#: ../../english/template/debian/links.tags.wml:143
msgid "Package Tracking System"
msgstr "Sustav praćenja paketa"

#: ../../english/template/debian/links.tags.wml:146
msgid "Debian Developer's Packages Overview"
msgstr "Pregled paketa Debian razvijatelja"

#: ../../english/template/debian/navbar.wml:10
#, fuzzy
#| msgid "Debian Project"
msgid "Debian Home"
msgstr "Debian Projekt"

#: ../../english/template/debian/recent_list.wml:7
msgid "No items for this year."
msgstr "Za ovu godinu nema članaka."

#: ../../english/template/debian/recent_list.wml:11
msgid "proposed"
msgstr "predloženo"

#: ../../english/template/debian/recent_list.wml:15
msgid "in discussion"
msgstr "u raspravi"

#: ../../english/template/debian/recent_list.wml:19
msgid "voting open"
msgstr "na glasanju"

#: ../../english/template/debian/recent_list.wml:23
msgid "finished"
msgstr "završeno"

#: ../../english/template/debian/recent_list.wml:26
msgid "withdrawn"
msgstr "povučeno"

#: ../../english/template/debian/recent_list.wml:30
msgid "Future events"
msgstr "Budući događaji"

#: ../../english/template/debian/recent_list.wml:33
msgid "Past events"
msgstr "Prošli događaji"

#: ../../english/template/debian/recent_list.wml:37
msgid "(new revision)"
msgstr "(revidirano)"

#: ../../english/template/debian/recent_list.wml:303
msgid "Report"
msgstr "Izvješće"

#. given a manual name and an architecture, join them
#. if you need to reorder the two, use "%2$s ... %1$s", cf. printf(3)
#: ../../english/template/debian/release.wml:7
msgid "<void id=\"doc_for_arch\" />%s for %s"
msgstr "<void id=\"doc_for_arch\" />%s za %s arhitekturu"

#: ../../english/template/debian/translation-check.wml:37
msgid ""
"<em>Note:</em> The <a href=\"$link\">original document</a> is newer than "
"this translation."
msgstr ""
"<em>Pažnja:</em> <a href=\"$link\">Originalni dokument</a> je noviji od ovog "
"prijevoda."

#: ../../english/template/debian/translation-check.wml:43
msgid ""
"Warning! This translation is too out of date, please see the <a href=\"$link"
"\">original</a>."
msgstr ""
"Pažnja! Ovaj prijevod je jako zastario, molimo pogledajte <a href=\"$link"
"\">original</a>."

#: ../../english/template/debian/translation-check.wml:49
msgid ""
"<em>Note:</em> The original document of this translation no longer exists."
msgstr "<em>Pažnja:</em> Originalni dokument za ovaj prijevod više ne postoji."

#: ../../english/template/debian/url.wml:4
msgid "URL"
msgstr "URL"

#: ../../english/template/debian/users.wml:7
msgid "Back to the <a href=\"../\">Who's using Debian? page</a>."
msgstr "Povratak na stranicu <a href=\"../\">Who's using Debian?</a>."

#~ msgid "Visit the site sponsor"
#~ msgstr "Posjetite pokrovitelja stranica"

#~ msgid "Select a server near you"
#~ msgstr "Izaberite vama blizak poslužitelj"

#~ msgid "More information:"
#~ msgstr "Više informacija:"

#~ msgid "Taken by:"
#~ msgstr "Zauzeo:"

#~ msgid "Nobody"
#~ msgstr "Nitko"

#~ msgid "Rating:"
#~ msgstr "Ocjena:"

#~ msgid "More information"
#~ msgstr "Podrobnije informacije"

#~ msgid "Upcoming Attractions"
#~ msgstr "Nadolazeće atrakcije"

#~ msgid "link may no longer be valid"
#~ msgstr "veza možda više ne radi"

#~ msgid "When"
#~ msgstr "Kada"

#~ msgid "Where"
#~ msgstr "Gdje"

#~ msgid "More Info"
#~ msgstr "Podrobnije informacije"

#~ msgid "Debian Involvement"
#~ msgstr "Upletenost Debiana"

#~ msgid "Main Coordinator"
#~ msgstr "Glavni koordinator"

#~ msgid "<th>Project</th><th>Coordinator</th>"
#~ msgstr "<th>Projekt</th><th>Koordinator</th>"

#~ msgid "Related Links"
#~ msgstr "Srodne veze"

#~ msgid "Latest News"
#~ msgstr "Najnovije vijesti"

#~ msgid "Download calendar entry"
#~ msgstr "Prijenos unosa u kalendar"

#~ msgid ""
#~ "Back to: other <a href=\"./\">Debian news</a> || <a href=\"m4_HOME/"
#~ "\">Debian Project homepage</a>."
#~ msgstr ""
#~ "Povratak na: druge <a href=\"./\">vijesti o Debianu</a> || <a href="
#~ "\"m4_HOME/\">početnu stranicu Debian projekta</a>."

#~ msgid "<get-var url /> (dead link)"
#~ msgstr "<get-var url /> (mrtav link)"

#~ msgid ""
#~ "To receive this newsletter bi-weekly in your mailbox, <a href=\"https://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "Kako bi dobili ove vijesti svako dva tjedna u svoj elektronski poštanski "
#~ "pretinac, <a href=\"https://lists.debian.org/debian-news/\">pretplatite "
#~ "se na debian-news mailing listu.</a>"

#~ msgid "<a href=\"../../\">Back issues</a> of this newsletter are available."
#~ msgstr "<a href=\"../../\">Prijašnja izdanja</a> ovih vijesti su dostupna."

#~ msgid ""
#~ "<void id=\"singular\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />Debian Project News uređuje <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"plural\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Debian Project News uređuju <a href=\"mailto:debian-"
#~ "publicity@lists.debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Project News was edited by "
#~ "<a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />Ovo izdanje Debian Project News je uredio <a href="
#~ "\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Project News was edited by <a "
#~ "href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Ovo izdanje Debian Project News su uredili <a href="
#~ "\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#~ msgid "<void id=\"singular\" />It was translated by %s."
#~ msgstr "<void id=\"singular\" />Preveo %s."

#~ msgid "<void id=\"plural\" />It was translated by %s."
#~ msgstr "<void id=\"plural\" />Preveli %s."

#~ msgid "<void id=\"singularfemale\" />It was translated by %s."
#~ msgstr "<void id=\"singularfemale\" />Prevela %s."

#~ msgid "<void id=\"pluralfemale\" />It was translated by %s."
#~ msgstr "<void id=\"pluralfemale\" />Prevele %s."

#~ msgid "List of Speakers"
#~ msgstr "Popis govornika"

#~ msgid "Back to the <a href=\"./\">Debian speakers page</a>."
#~ msgstr "Natrag na stranicu <a href=\"./\">Debian govornici</a>."

#~ msgid ""
#~ "To receive this newsletter weekly in your mailbox, <a href=\"https://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "Kako bi dobili ove vijesti tjedno u svoj elektronski poštanski pretinac, "
#~ "<a href=\"https://lists.debian.org/debian-news/\">pretplatite se na "
#~ "debian-news mailing listu.</a>"

#~ msgid ""
#~ "<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />Debian Weekly News uređuje <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Debian Weekly News uređuju <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />Ovo izdanje Debian Weekly News je uredio <a href="
#~ "\"mailto:dwn@debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Ovo izdanje Debian Weekly News su uredili <a href="
#~ "\"mailto:dwn@debian.org\">%s</a>."

#~ msgid "&middot;"
#~ msgstr "&middot;"

#~ msgid "Download with Jigdo"
#~ msgstr "Download Jigdom"

#~ msgid "Download via HTTP/FTP"
#~ msgstr "Download HTTP-om/FTP-om"

#~ msgid "Buy CDs or DVDs"
#~ msgstr "Kupi CD-e ili DVD-e"

#~ msgid "Network Install"
#~ msgstr "Mrežna instalacija"

#~ msgid "<void id=\"dc_download\" />Download"
#~ msgstr "<void id=\"dc_download\" />Download"

#~ msgid "<void id=\"dc_misc\" />Misc"
#~ msgstr "<void id=\"dc_misc\" />Razno"

#~ msgid "<void id=\"dc_artwork\" />Artwork"
#~ msgstr "<void id=\"dc_artwork\" />Artwork"

#~ msgid "<void id=\"dc_mirroring\" />Mirroring"
#~ msgstr "<void id=\"dc_mirroring\" />Mirroranje"

#~ msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
#~ msgstr "<void id=\"dc_rsyncmirrors\" />Rsync mirrori"

#~ msgid "<void id=\"dc_torrent\" />Download with Torrent"
#~ msgstr "<void id=\"dc_torrent\" />Download Torrentom"

#~ msgid "<void id=\"dc_relinfo\" />Image Release Info"
#~ msgstr "<void id=\"dc_relinfo\" />Informacije o izdanju snimki"

#~ msgid "Debian CD team"
#~ msgstr "Debian CD grupa"

#~ msgid "debian_on_cd"
#~ msgstr "debian_na_cdu"

#~ msgid "<void id=\"faq-bottom\" />faq"
#~ msgstr "<void id=\"faq-bottom\" />faq"

#~ msgid "jigdo"
#~ msgstr "jigdo"

#~ msgid "http_ftp"
#~ msgstr "http_ftp"

#~ msgid "buy"
#~ msgstr "kupi"

#~ msgid "net_install"
#~ msgstr "mrežni_instal"

#~ msgid "<void id=\"misc-bottom\" />misc"
#~ msgstr "razno"

#~ msgid ""
#~ "English-language <a href=\"/MailingLists/disclaimer\">public mailing "
#~ "list</a> for CDs/DVDs:"
#~ msgstr ""
#~ "<a href=\"/MailingLists/disclaimer\">Javna mailing lista</a> na engleskom "
#~ "jeziku za CD-e/DVD-e:"

#~ msgid "Date"
#~ msgstr "Datum"

#~ msgid "Time Line"
#~ msgstr "Vremenski tok"

#~ msgid "Nominations"
#~ msgstr "Nominacije"

#~ msgid "Debate"
#~ msgstr "Rasprava"

#~ msgid "Platforms"
#~ msgstr "Platforme"

#~ msgid "Proposer"
#~ msgstr "Predlagač"

#~ msgid "Proposal A Proposer"
#~ msgstr "Predlagač prijedloga A"

#~ msgid "Proposal B Proposer"
#~ msgstr "Predlagač prijedloga B"

#~ msgid "Proposal C Proposer"
#~ msgstr "Predlagač prijedloga C"

#~ msgid "Proposal D Proposer"
#~ msgstr "Predlagač prijedloga D"

#~ msgid "Proposal E Proposer"
#~ msgstr "Predlagač prijedloga E"

#~ msgid "Proposal F Proposer"
#~ msgstr "Predlagač prijedloga F"

#~ msgid "Seconds"
#~ msgstr "Podržavaju"

#~ msgid "Proposal A Seconds"
#~ msgstr "Prijedlog A podržavaju"

#~ msgid "Proposal B Seconds"
#~ msgstr "Prijedlog B podržavaju"

#~ msgid "Proposal C Seconds"
#~ msgstr "Prijedlog C podržavaju"

#~ msgid "Proposal D Seconds"
#~ msgstr "Prijedlog D podržavaju"

#~ msgid "Proposal E Seconds"
#~ msgstr "Prijedlog E podržavaju"

#~ msgid "Proposal F Seconds"
#~ msgstr "Prijedlog F podržavaju"

#~ msgid "Opposition"
#~ msgstr "Protivljenje"

#~ msgid "Text"
#~ msgstr "Tekst"

#~ msgid "Proposal A"
#~ msgstr "Prijedlog A"

#~ msgid "Proposal B"
#~ msgstr "Prijedlog B"

#~ msgid "Proposal C"
#~ msgstr "Prijedlog C"

#~ msgid "Proposal D"
#~ msgstr "Prijedlog D"

#~ msgid "Proposal E"
#~ msgstr "Prijedlog E"

#~ msgid "Proposal F"
#~ msgstr "Prijedlog F"

#~ msgid "Choices"
#~ msgstr "Izbor"

#~ msgid "Amendment Proposer"
#~ msgstr "Predlagač amandmana"

#~ msgid "Amendment Seconds"
#~ msgstr "Amandman podržavaju"

#~ msgid "Amendment Text"
#~ msgstr "Tekst amandmana"

#~ msgid "Amendment Proposer A"
#~ msgstr "Amandman A predlaže"

#~ msgid "Amendment Seconds A"
#~ msgstr "Amandman A podržavaju"

#~ msgid "Amendment Text A"
#~ msgstr "Tekst amandmana A"

#~ msgid "Amendment Proposer B"
#~ msgstr "Amandman B predlaže"

#~ msgid "Amendment Seconds B"
#~ msgstr "Amandman B podržavaju"

#~ msgid "Amendment Text B"
#~ msgstr "Tekst amandmana B"

#~ msgid "Amendments"
#~ msgstr "Amandmani"

#~ msgid "Proceedings"
#~ msgstr "Postupak"

#~ msgid "Majority Requirement"
#~ msgstr "Zahtjevi za većinu"

#~ msgid "Data and Statistics"
#~ msgstr "Podaci i statistike"

#~ msgid "Quorum"
#~ msgstr "Kvorum"

#~ msgid "Minimum Discussion"
#~ msgstr "Minimalna rasprava"

#~ msgid "Ballot"
#~ msgstr "Glasački list"

#~ msgid "Forum"
#~ msgstr "Forum"

#~ msgid "Outcome"
#~ msgstr "Rezultat"

#~ msgid "Waiting&nbsp;for&nbsp;Sponsors"
#~ msgstr "Čeka&nbsp;na&nbsp;pokrovitelje"

#~ msgid "In&nbsp;Discussion"
#~ msgstr "U&nbsp;raspravi"

#~ msgid "Voting&nbsp;Open"
#~ msgstr "Glasovanje&nbsp;otvoreno"

#~ msgid "Decided"
#~ msgstr "Odlučeno"

#~ msgid "Withdrawn"
#~ msgstr "Povučeno"

#~ msgid "Other"
#~ msgstr "Ostalo"

#~ msgid "Home&nbsp;Vote&nbsp;Page"
#~ msgstr "Početna&nbsp;stranica&nbsp;glasovanja"

#~ msgid "How&nbsp;To"
#~ msgstr "Kako"

#~ msgid "Submit&nbsp;a&nbsp;Proposal"
#~ msgstr "Predati&nbsp;prijedlog"

#~ msgid "Amend&nbsp;a&nbsp;Proposal"
#~ msgstr "Ispraviti&nbsp;prijedlog"

#~ msgid "Follow&nbsp;a&nbsp;Proposal"
#~ msgstr "Slijediti&nbsp;prijedlog"

#~ msgid "Read&nbsp;a&nbsp;Result"
#~ msgstr "Pročitati&nbsp;rezultat"

#~ msgid "Vote"
#~ msgstr "Glasovati"

#~ msgid ""
#~ "See the <a href=\"./\">license information</a> page for an overview of "
#~ "the Debian License Summaries (DLS)."
#~ msgstr ""
#~ "Pogledajte stranicu <a href=\"./\">informacije o licencama</a> za pregled "
#~ "Debian License Summaries (DLS)."

#~ msgid "Not Redistributable"
#~ msgstr "Ne može se redistribuirati"

#~ msgid "Non-Free"
#~ msgstr "Neslobodan"

#~ msgid "Free"
#~ msgstr "Slobodan"

#~ msgid "not redistributable"
#~ msgstr "ne može se redistribuirati"

#~ msgid "non-free"
#~ msgstr "neslobodan"

#~ msgid "free"
#~ msgstr "slobodan"

#~ msgid "License text"
#~ msgstr "Tekst licence"

#~ msgid "License text (translated)"
#~ msgstr "Tekst licence (preveden)"

#~ msgid "This summary was prepared by <summary-author/>."
#~ msgstr "Ovaj sažetak je pripremio <summary-author/>."

#~ msgid ""
#~ "The original summary by <summary-author/> can be found in the <a href="
#~ "\"<summary-url/>\">list archives</a>."
#~ msgstr ""
#~ "Originalni sažetak kojeg je napisao <summary-author/> može se naći u <a "
#~ "href=\"<summary-url/>\">arhivama mailing lista</a>."

#~ msgid "Original Summary"
#~ msgstr "Originalni sažetak"

#~ msgid "Discussion"
#~ msgstr "Rasprava"

#~ msgid "Justification"
#~ msgstr "Opravdanje"

#~ msgid "Summary"
#~ msgstr "Sažetak"

#~ msgid "Version"
#~ msgstr "Inačica"

#~ msgid "License"
#~ msgstr "Licenca"

#~ msgid "Date published"
#~ msgstr "Datum objave"

#~ msgid "%s  &ndash; %s, Version %s: %s"
#~ msgstr "%s  &ndash; %s, inačica %s: %s"

#~ msgid "%s  &ndash; %s: %s"
#~ msgstr "%s  &ndash; %s: %s"

#~ msgid "Debian-Legal Archive"
#~ msgstr "Debian-Legal arhiva"

#~ msgid "DFSG FAQ"
#~ msgstr "DFSG FAQ"

#~ msgid "DFSG"
#~ msgstr "DFSG"

#~ msgid "DLS Index"
#~ msgstr "DLS indeks"

#~ msgid "License Information"
#~ msgstr "Informacije o licenci"

#~ msgid "Back to the <a href=\"./\">Debian consultants page</a>."
#~ msgstr "Natrag na stranicu <a href=\"./\">Debian konzultanti</a>."

#~ msgid "List of Consultants"
#~ msgstr "Popis konzultanata"
