# Translation of organization.sk.po
# Copyright (C)
# This file is distributed under the same license as the webwml package.
# Ivan Masár <helix84@centrum.sk>, 2009, 2011, 2013, 2014, 2017.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"Report-Msgid-Bugs-To: debian-www@lists.debian.org\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2017-07-12 16:39+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: x\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Virtaal 0.7.1\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "delegácia pošty"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "email schôdzky"

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegát"

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"male\"/>delegátka"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "aktuálne"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "člen"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "manažér"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:34
msgid "Stable Release Manager"
msgstr "Správca stabilného vydania"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr "wizard"

#: ../../english/intro/organization.data:39
msgid "chairman"
msgstr "predseda"

#: ../../english/intro/organization.data:42
msgid "assistant"
msgstr "asistent"

#: ../../english/intro/organization.data:44
msgid "secretary"
msgstr "sekretár"

#: ../../english/intro/organization.data:53
#: ../../english/intro/organization.data:63
msgid "Officers"
msgstr "Predstavenstvo"

#: ../../english/intro/organization.data:54
#: ../../english/intro/organization.data:80
msgid "Distribution"
msgstr "Distribúcia"

#: ../../english/intro/organization.data:55
#: ../../english/intro/organization.data:224
msgid "Communication and Outreach"
msgstr "Komunikácia a PR"

#: ../../english/intro/organization.data:56
#: ../../english/intro/organization.data:227
#| msgid "Publicity"
msgid "Publicity team"
msgstr "Marketingový tím"

#: ../../english/intro/organization.data:57
#: ../../english/intro/organization.data:295
msgid "Support and Infrastructure"
msgstr "Podpora a infraštruktúra"

#. formerly Custom Debian Distributions (CCDs); see https://blends.debian.org/blends/ch-about.en.html#s-Blends
#: ../../english/intro/organization.data:59
msgid "Debian Pure Blends"
msgstr "Debian Pure Blends"

#: ../../english/intro/organization.data:66
msgid "Leader"
msgstr "Vedúci"

#: ../../english/intro/organization.data:68
msgid "Technical Committee"
msgstr "Technická komisia"

#: ../../english/intro/organization.data:75
msgid "Secretary"
msgstr "Sekretár"

#: ../../english/intro/organization.data:83
msgid "Development Projects"
msgstr "Vývojové projekty"

#: ../../english/intro/organization.data:84
msgid "FTP Archives"
msgstr "FTP archívy"

#: ../../english/intro/organization.data:86
msgid "FTP Masters"
msgstr "FTP Masters"

#: ../../english/intro/organization.data:90
msgid "FTP Assistants"
msgstr "FTP asistenti"

#: ../../english/intro/organization.data:99
msgid "FTP Wizards"
msgstr "FTP Wizards"

#: ../../english/intro/organization.data:103
msgid "Backports"
msgstr "Spätné porty"

#: ../../english/intro/organization.data:105
msgid "Backports Team"
msgstr "Tím spätných portov"

#: ../../english/intro/organization.data:109
msgid "Individual Packages"
msgstr "Jednotlivé balíky"

#: ../../english/intro/organization.data:110
msgid "Release Management"
msgstr "Správa vydaní"

#: ../../english/intro/organization.data:112
msgid "Release Team"
msgstr "Tím vydania"

#: ../../english/intro/organization.data:125
msgid "Quality Assurance"
msgstr "Zabezpečenie kvality"

#: ../../english/intro/organization.data:126
msgid "Installation System Team"
msgstr "Tím inštalátora"

#: ../../english/intro/organization.data:127
msgid "Release Notes"
msgstr "Poznámky k vydaniu"

#: ../../english/intro/organization.data:129
msgid "CD Images"
msgstr "Obrazy CD"

#: ../../english/intro/organization.data:131
msgid "Production"
msgstr "Produkcia"

#: ../../english/intro/organization.data:139
msgid "Testing"
msgstr "Testovanie"

#: ../../english/intro/organization.data:141
msgid "Autobuilding infrastructure"
msgstr "Infraštruktúra automatického zostavovania"

#: ../../english/intro/organization.data:143
msgid "Wanna-build team"
msgstr "Tím wanna-build"

#: ../../english/intro/organization.data:151
msgid "Buildd administration"
msgstr "Správa buildd"

#: ../../english/intro/organization.data:170
msgid "Documentation"
msgstr "Dokumentácia"

#: ../../english/intro/organization.data:175
msgid "Work-Needing and Prospective Packages list"
msgstr "Balíky, ktoré potrebujú pomoc a perspektívne balíky"

#: ../../english/intro/organization.data:178
msgid "Live System Team"
msgstr "Tím Live systému"

#: ../../english/intro/organization.data:179
msgid "Ports"
msgstr "Porty"

#: ../../english/intro/organization.data:214
msgid "Special Configurations"
msgstr "Špeciálne konfigurácie"

#: ../../english/intro/organization.data:217
msgid "Laptops"
msgstr "Notebooky"

#: ../../english/intro/organization.data:218
msgid "Firewalls"
msgstr "Firewally"

#: ../../english/intro/organization.data:219
msgid "Embedded systems"
msgstr "Vnorené systémy"

#: ../../english/intro/organization.data:232
msgid "Press Contact"
msgstr "Tlačový kontakt"

#: ../../english/intro/organization.data:234
msgid "Web Pages"
msgstr "Webstránky"

#: ../../english/intro/organization.data:244
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:249
msgid "Outreach"
msgstr "PR"

#: ../../english/intro/organization.data:253
msgid "Debian Women Project"
msgstr "Projekt Ženy Debianu"

#: ../../english/intro/organization.data:261
msgid "Anti-harassment"
msgstr "Boj proti obťažovaniu"

#: ../../english/intro/organization.data:267
msgid "Events"
msgstr "Udalosti"

#: ../../english/intro/organization.data:273
#| msgid "Technical Committee"
msgid "DebConf Committee"
msgstr "Komisia DebConf"

#: ../../english/intro/organization.data:280
msgid "Partner Program"
msgstr "Partnerský program"

#: ../../english/intro/organization.data:285
msgid "Hardware Donations Coordination"
msgstr "Koordinácia darov hardvéru"

#: ../../english/intro/organization.data:298
msgid "User support"
msgstr "Používateľská podpora"

#: ../../english/intro/organization.data:365
msgid "Bug Tracking System"
msgstr "Systém sledovania chýb"

#: ../../english/intro/organization.data:370
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Správa konferencií a archívov konferencií"

#: ../../english/intro/organization.data:378
msgid "New Members Front Desk"
msgstr "Recepcia nových členov"

#: ../../english/intro/organization.data:384
msgid "Debian Account Managers"
msgstr "Správcovia účtov Debianu"

#: ../../english/intro/organization.data:389
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Ak chcete poslať súkromnú správu všetkým DAM, použite kľúč GPG "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:390
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Správcovia kľúčov (PGP a GPG)"

#: ../../english/intro/organization.data:393
msgid "Security Team"
msgstr "Bezpečnostný tím"

#: ../../english/intro/organization.data:406
msgid "Consultants Page"
msgstr "Stránka konzultantov"

#: ../../english/intro/organization.data:411
msgid "CD Vendors Page"
msgstr "Stránka dodávateľov CD"

#: ../../english/intro/organization.data:414
msgid "Policy"
msgstr "Politika"

#: ../../english/intro/organization.data:419
msgid "System Administration"
msgstr "Správa systému"

#: ../../english/intro/organization.data:420
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Túto adresu použite, ak sa stretnete s problémami na niektorom so strojov "
"Debianu, aj v prípade že na nich potrebujete nainštalovať balík alebo máte "
"problém s heslom."

#: ../../english/intro/organization.data:430
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Ak máte hardvérový problém so strojom Debianu, pozrite stránku <a href="
"\"https://db.debian.org/machines.cgi\">Strojov Debianu</a>, mala by "
"obsahovať informácie o správcovi každého zo strojov."

#: ../../english/intro/organization.data:431
msgid "LDAP Developer Directory Administrator"
msgstr "Správca LDAP adresára vývojárov"

#: ../../english/intro/organization.data:432
msgid "Mirrors"
msgstr "Zrkadlá"

#: ../../english/intro/organization.data:437
msgid "DNS Maintainer"
msgstr "Správca DNS"

#: ../../english/intro/organization.data:438
msgid "Package Tracking System"
msgstr "Systém sledovania balíkov"

#: ../../english/intro/organization.data:440
msgid "Auditor"
msgstr "Auditor"

#: ../../english/intro/organization.data:446
#| msgid "<a href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Žiadosti o použitie <a name=\"trademark\" href=\"m4_HOME/trademark\">"
"ochranných známok</a>"

#: ../../english/intro/organization.data:449
msgid "Alioth administrators"
msgstr "Správcovia Alioth"

#: ../../english/intro/organization.data:462
msgid "Debian for children from 1 to 99"
msgstr "Debian pre deti od 1 do 99"

#: ../../english/intro/organization.data:465
msgid "Debian for medical practice and research"
msgstr "Debian pre medicínsku prax a výskum"

#: ../../english/intro/organization.data:468
msgid "Debian for education"
msgstr "Debian pre vzdelávanie"

#: ../../english/intro/organization.data:473
msgid "Debian in legal offices"
msgstr "Debian v právnických kanceláriách"

#: ../../english/intro/organization.data:477
msgid "Debian for people with disabilities"
msgstr "Debian pre ľudí s postihnutiami"

#: ../../english/intro/organization.data:481
msgid "Debian for science and related research"
msgstr "Debian pre vedu a súvisiaci výskum"

#~ msgid "Publicity"
#~ msgstr "Publicita"

#~ msgid "Bits from Debian"
#~ msgstr "Čriepky z Debianu"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Tím správcov zväzku kľúčov správcov Debianu (DM)"

#~ msgid "DebConf chairs"
#~ msgstr "Predsedovia DebConf"

#~ msgid "Volatile Team"
#~ msgstr "Tím volatile"

#~ msgid "Vendors"
#~ msgstr "Dodávatelia"

#~ msgid "Handhelds"
#~ msgstr "Handheldy"

#~ msgid "Marketing Team"
#~ msgstr "Marketingový tím"

#~ msgid "Key Signing Coordination"
#~ msgstr "Koordinácia podpisovania kľúčov"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Prispôsobené distribúcie Debianu"

#~ msgid "Release Team for ``stable''"
#~ msgstr "Tím vydania „stable“"

#~ msgid "Accountant"
#~ msgstr "Účtovník"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Univerzálny operačný systém pre pracovnú stanicu"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian pre neziskové organizácie"

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "Správcov zodpovedajúcich za buildd konkrétnej architekúry môžete "
#~ "zastihnúť na <genericemail arch@buildd.debian.org>, napr. <genericemail "
#~ "i386@buildd.debian.org>."

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "Mená správcov jednotlivých buildd nájdete tiež na <a href=\"http://www."
#~ "buildd.net\">http://www.buildd.net</a>.  Vyberte si architektúru a "
#~ "distribúciu pre ktorú chcete zobraziť správcov buildd."

#~ msgid "current Debian Project Leader"
#~ msgstr "súčasný líder projektu Debian"

#~ msgid "Security Audit Project"
#~ msgstr "Projekt Bezpečnostný audit"

#~ msgid "Testing Security Team"
#~ msgstr "Bezpečnostný tím pre „testing“"
