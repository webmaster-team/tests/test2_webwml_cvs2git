# Copyright (C) 2008, 2012, 2014 Debian French l10n team <debian-l10n-french@lists.debian.org>
#
# Pierre Machard <pmachard@debian.org>, 2008.
# David Prévot <david@tilapin.org>, 2012, 2014.
# Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fichier>, 2017
msgid ""
msgstr ""
"Project-Id-Version: debian webwml other\n"
"PO-Revision-Date: 2017-05-10 23:02+0200\n"
"Last-Translator: David Prévot <david@tilapin.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"

#: ../../english/banners/index.tags:7
msgid "Download"
msgstr "Téléchargement"

#: ../../english/banners/index.tags:11
msgid "Old banner ads"
msgstr "Vieilles bannières publicitaires"

#: ../../english/devel/debian-installer/ports-status.defs:10
msgid "Working"
msgstr "Fonctionne"

#: ../../english/devel/debian-installer/ports-status.defs:20
msgid "sarge"
msgstr "Sarge"

#: ../../english/devel/debian-installer/ports-status.defs:30
msgid "sarge (broken)"
msgstr "Sarge (cassée)"

#: ../../english/devel/debian-installer/ports-status.defs:40
msgid "Booting"
msgstr "Démarre"

#: ../../english/devel/debian-installer/ports-status.defs:50
msgid "Building"
msgstr "Se construit"

#: ../../english/devel/debian-installer/ports-status.defs:56
msgid "Not yet"
msgstr "Pas encore"

#: ../../english/devel/debian-installer/ports-status.defs:59
msgid "No kernel"
msgstr "Pas de noyau"

#: ../../english/devel/debian-installer/ports-status.defs:62
msgid "No images"
msgstr "Pas d'images"

#: ../../english/devel/debian-installer/ports-status.defs:65
msgid "<void id=\"d-i\" />Unknown"
msgstr "<void id=\"d-i\" />Inconnu"

#: ../../english/devel/debian-installer/ports-status.defs:68
msgid "Unavailable"
msgstr "Non disponible"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "Le coin des nouveaux membres"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "Étape 1"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "Étape 2"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "Étape 3"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "Étape 4"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "Étape 5"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "Étape 6"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "Étape 7"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "Liste de contrôle pour les candidats"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"Voir <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</"
"a> pour plus de renseignements (seulement disponible en français)."

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
msgid "More information"
msgstr "Plus de renseignements"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"Voir <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> pour plus de renseignements (seulement disponible en espagnol)."

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "Téléphone"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "Fax"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "Adresse"

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "Avec « Debian »"

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "Sans « Debian »"

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "PostScript encapsulé"

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[Fonctionnant sous Debian]"

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[Fonctionnant sous Debian GNU/Linux]"

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "[Fonctionnant sous Debian]"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Debian] (mini-bouton)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "identique à ci-dessus"

#: ../../english/misc/merchandise.def:8
msgid "Products"
msgstr "Produits"

#: ../../english/misc/merchandise.def:11
msgid "T-shirts"
msgstr "T-shirts"

#: ../../english/misc/merchandise.def:14
msgid "hats"
msgstr "casquettes"

#: ../../english/misc/merchandise.def:17
msgid "stickers"
msgstr "autocollants"

#: ../../english/misc/merchandise.def:20
msgid "mugs"
msgstr "tasses"

#: ../../english/misc/merchandise.def:23
msgid "other clothing"
msgstr "autres vêtements"

#: ../../english/misc/merchandise.def:26
msgid "polo shirts"
msgstr "chemises polo"

#: ../../english/misc/merchandise.def:29
msgid "frisbees"
msgstr "frisbees"

#: ../../english/misc/merchandise.def:32
msgid "mouse pads"
msgstr "tapis de souris"

#: ../../english/misc/merchandise.def:35
msgid "badges"
msgstr "écussons"

#: ../../english/misc/merchandise.def:38
msgid "basketball goals"
msgstr "paniers de basket"

#: ../../english/misc/merchandise.def:42
msgid "earrings"
msgstr "boucles d'oreille"

#: ../../english/misc/merchandise.def:45
msgid "suitcases"
msgstr "valises"

#: ../../english/misc/merchandise.def:48
msgid "umbrellas"
msgstr "parapluies"

#: ../../english/misc/merchandise.def:51
msgid "pillowcases"
msgstr "taies d'oreiller"

#: ../../english/misc/merchandise.def:54
msgid "keychains"
msgstr "porte-clefs"

#: ../../english/misc/merchandise.def:57
msgid "Swiss army knives"
msgstr "couteaux suisses"

#: ../../english/misc/merchandise.def:60
msgid "USB-Sticks"
msgstr "clefs USB"

#: ../../english/misc/merchandise.def:75
msgid "lanyards"
msgstr "cordons"

#: ../../english/misc/merchandise.def:78
msgid "others"
msgstr "autres"

#: ../../english/misc/merchandise.def:98
msgid "Donates money to Debian"
msgstr "Don pécuniaire à Debian"

#: ../../english/misc/merchandise.def:102
msgid "Money is used to organize local free software events"
msgstr ""
"Argent utilisé pour organiser des évènements locaux à propos du logiciel "
"libre"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr "Depuis combien de temps utilisez-vous Debian ?"

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr "Êtes-vous développeur Debian ?"

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr "Dans quelles parties de Debian êtes-vous impliquée ?"

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr "Pour quelles raisons avez-vous voulu travailler dans Debian ?"

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""
"Avez-vous des conseils pour les femmes qui désirent s'impliquer davantage "
"dans Debian ?"

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr ""
"Êtes-vous impliquée dans des groupes techniques avec d'autres femmes ? "
"Lesquels ?"

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr "Un peu plus sur vous…"

#: ../../english/y2k/l10n.data:6
msgid "OK"
msgstr "OK"

#: ../../english/y2k/l10n.data:9
msgid "BAD"
msgstr "MAL"

#: ../../english/y2k/l10n.data:12
msgid "OK?"
msgstr "OK ?"

#: ../../english/y2k/l10n.data:15
msgid "BAD?"
msgstr "MAL ?"

#: ../../english/y2k/l10n.data:18
msgid "??"
msgstr "??"

#: ../../english/y2k/l10n.data:21
msgid "Unknown"
msgstr "Inconnu"

#: ../../english/y2k/l10n.data:24
msgid "ALL"
msgstr "TOUS"

#: ../../english/y2k/l10n.data:27
msgid "Package"
msgstr "Paquet"

#: ../../english/y2k/l10n.data:30
msgid "Status"
msgstr "État"

#: ../../english/y2k/l10n.data:33
msgid "Version"
msgstr "Version"

#: ../../english/y2k/l10n.data:36
msgid "URL"
msgstr "URL"

#~ msgid "Wanted:"
#~ msgstr "Désiré :"

#~ msgid "Who:"
#~ msgstr "Demandeur :"

#~ msgid "Architecture:"
#~ msgstr "Architectures :"

#~ msgid "Specifications:"
#~ msgstr "Spécifications :"

#~ msgid "Where:"
#~ msgstr "Localisation :"
