#
# Trần Ngọc Quân <vnwildman@gmail.com>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml legal\n"
"PO-Revision-Date: 2015-06-20 08:12+0700\n"
"Last-Translator: Trần Ngọc Quân <vnwildman@gmail.com>\n"
"Language-Team: Vietnamese <debian-l10n-vietnamese@lists.debian.org>\n"
"Language: vi\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Gtranslator 2.91.7\n"

#: ../../english/template/debian/legal.wml:15
msgid "License Information"
msgstr "Thông tin giấy phép"

#: ../../english/template/debian/legal.wml:19
msgid "DLS Index"
msgstr "Mục lục DLS"

#: ../../english/template/debian/legal.wml:23
msgid "DFSG"
msgstr "DFSG"

#: ../../english/template/debian/legal.wml:27
msgid "DFSG FAQ"
msgstr "DFSG FAQ"

#: ../../english/template/debian/legal.wml:31
msgid "Debian-Legal Archive"
msgstr "Kho lưu điều khoản Debian"

#. title string without version, on the form "dls-xxx - license name: status"
#: ../../english/template/debian/legal.wml:49
msgid "%s  &ndash; %s: %s"
msgstr "%s  &ndash; %s: %s"

#. title string with version, on the form "dls-xxx - license name, version: status"
#: ../../english/template/debian/legal.wml:52
msgid "%s  &ndash; %s, Version %s: %s"
msgstr "%s  &ndash; %s, Phiên bản %s: %s"

#: ../../english/template/debian/legal.wml:59
msgid "Date published"
msgstr "Ngày xuất bản"

#: ../../english/template/debian/legal.wml:61
msgid "License"
msgstr "Giấy phép"

#: ../../english/template/debian/legal.wml:64
msgid "Version"
msgstr "Phiên bản"

#: ../../english/template/debian/legal.wml:66
msgid "Summary"
msgstr "Tóm tắt"

#: ../../english/template/debian/legal.wml:70
msgid "Justification"
msgstr "Cân chỉnh"

#: ../../english/template/debian/legal.wml:72
msgid "Discussion"
msgstr "Thảo luận"

#: ../../english/template/debian/legal.wml:74
msgid "Original Summary"
msgstr "Tóm tắt gốc"

#: ../../english/template/debian/legal.wml:76
msgid ""
"The original summary by <summary-author/> can be found in the <a href="
"\"<summary-url/>\">list archives</a>."
msgstr ""
"Tóm tắt gốc bởi <summary-author/> có thể tìm thấy tại <a href=\"<summary-url/"
">\">phần lưu trữ bó thư</a>."

#: ../../english/template/debian/legal.wml:77
msgid "This summary was prepared by <summary-author/>."
msgstr "Tóm tắt này được chuẩn bị bởi <summary-author/>."

#: ../../english/template/debian/legal.wml:80
msgid "License text (translated)"
msgstr "Văn bản giấy phép (bản dịch)"

#: ../../english/template/debian/legal.wml:83
msgid "License text"
msgstr "Văn bản giấy phép"

#: ../../english/template/debian/legal_tags.wml:6
msgid "free"
msgstr "tự do"

#: ../../english/template/debian/legal_tags.wml:7
msgid "non-free"
msgstr "không tự do"

#: ../../english/template/debian/legal_tags.wml:8
msgid "not redistributable"
msgstr "không tái phân phối"

#. For the use in headlines, see legal/licenses/byclass.wml
#: ../../english/template/debian/legal_tags.wml:12
msgid "Free"
msgstr "Tự do"

#: ../../english/template/debian/legal_tags.wml:13
msgid "Non-Free"
msgstr "Không tự do"

#: ../../english/template/debian/legal_tags.wml:14
msgid "Not Redistributable"
msgstr "Không tái phân phối"

#: ../../english/template/debian/legal_tags.wml:27
msgid ""
"See the <a href=\"./\">license information</a> page for an overview of the "
"Debian License Summaries (DLS)."
msgstr ""
"Xem trang <a href=\"./\">thông tin giấy phép</a> để có thể có cái nhìn tổng "
"quan về Tóm tắt sơ lược giấy phép Debian (DLS)."
