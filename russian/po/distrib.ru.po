# translation of distrib.ru.po to Russian
# Yuri Kozlov <yuray@komyakino.ru>, 2009, 2011, 2013.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml distrib\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-04-12 00:19+0500\n"
"Last-Translator: Lev Lamberov\n"
"Language-Team: Russian <>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7.1\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../english/distrib/search_contents-form.inc:9
#: ../../english/distrib/search_packages-form.inc:8
msgid "Keyword"
msgstr "Слово для поиска"

#: ../../english/distrib/search_contents-form.inc:13
msgid "Display"
msgstr "Показать"

#: ../../english/distrib/search_contents-form.inc:16
msgid "paths ending with the keyword"
msgstr "пути, заканчивающиеся искомым словом"

#: ../../english/distrib/search_contents-form.inc:19
msgid "packages that contain files named like this"
msgstr "пакеты, содержащие файлы с таким именем"

#: ../../english/distrib/search_contents-form.inc:22
msgid "packages that contain files whose names contain the keyword"
msgstr "пакеты, содержащие файлы, имена которых содержат искомое слово"

#: ../../english/distrib/search_contents-form.inc:25
#: ../../english/distrib/search_packages-form.inc:25
msgid "Distribution"
msgstr "Дистрибутив"

#: ../../english/distrib/search_contents-form.inc:27
#: ../../english/distrib/search_packages-form.inc:27
msgid "experimental"
msgstr "экспериментальный"

#: ../../english/distrib/search_contents-form.inc:28
#: ../../english/distrib/search_packages-form.inc:28
msgid "unstable"
msgstr "нестабильный"

#: ../../english/distrib/search_contents-form.inc:29
#: ../../english/distrib/search_packages-form.inc:29
msgid "testing"
msgstr "тестируемый"

#: ../../english/distrib/search_contents-form.inc:30
#: ../../english/distrib/search_packages-form.inc:30
msgid "stable"
msgstr "стабильный"

#: ../../english/distrib/search_contents-form.inc:31
#: ../../english/distrib/search_packages-form.inc:31
msgid "oldstable"
msgstr "предыдущий стабильный"

#: ../../english/distrib/search_contents-form.inc:33
msgid "Architecture"
msgstr "архитектура"

#: ../../english/distrib/search_contents-form.inc:38
#: ../../english/distrib/search_packages-form.inc:32
#: ../../english/distrib/search_packages-form.inc:39
msgid "any"
msgstr "где угодно"

#: ../../english/distrib/search_contents-form.inc:48
#: ../../english/distrib/search_packages-form.inc:43
msgid "Search"
msgstr "Искать"

#: ../../english/distrib/search_contents-form.inc:49
#: ../../english/distrib/search_packages-form.inc:44
msgid "Reset"
msgstr "Сброс"

#: ../../english/distrib/search_packages-form.inc:12
msgid "Search on"
msgstr "Искать в"

#: ../../english/distrib/search_packages-form.inc:14
msgid "Package names only"
msgstr "именах пакетов"

#: ../../english/distrib/search_packages-form.inc:16
msgid "Descriptions"
msgstr "описаниях"

#: ../../english/distrib/search_packages-form.inc:18
msgid "Source package names"
msgstr "именах пакетов исходного кода"

#: ../../english/distrib/search_packages-form.inc:21
msgid "Only show exact matches"
msgstr "Показывать только точные совпадения"

#: ../../english/distrib/search_packages-form.inc:34
msgid "Section"
msgstr "Секция"

#: ../../english/distrib/search_packages-form.inc:36
msgid "main"
msgstr "основная"

#: ../../english/distrib/search_packages-form.inc:37
msgid "contrib"
msgstr "добавочная"

#: ../../english/distrib/search_packages-form.inc:38
msgid "non-free"
msgstr "несвободная"

#: ../../english/releases/arches.data:8
msgid "Alpha"
msgstr "Alpha"

#: ../../english/releases/arches.data:9
msgid "64-bit PC (amd64)"
msgstr "64-битный ПК (amd64)"

#: ../../english/releases/arches.data:10
msgid "ARM"
msgstr "ARM"

#: ../../english/releases/arches.data:11
msgid "64-bit ARM (AArch64)"
msgstr "64-битный ARM (AArch64)"

#: ../../english/releases/arches.data:12
msgid "EABI ARM (armel)"
msgstr "EABI ARM (armel)"

#: ../../english/releases/arches.data:13
msgid "Hard Float ABI ARM (armhf)"
msgstr "ABI ARM с аппаратной поддержкой чисел с плавающей запятой (armhf)"

#: ../../english/releases/arches.data:14
msgid "HP PA-RISC"
msgstr "HP PA-RISC"

#: ../../english/releases/arches.data:15
msgid "Hurd 32-bit PC (i386)"
msgstr "Hurd 32-битный ПК (i386)"

#: ../../english/releases/arches.data:16
msgid "32-bit PC (i386)"
msgstr "32-битный ПК (i386)"

#: ../../english/releases/arches.data:17
msgid "Intel Itanium IA-64"
msgstr "Intel Itanium IA-64"

#: ../../english/releases/arches.data:18
msgid "kFreeBSD 32-bit PC (i386)"
msgstr "kFreeBSD 32-битный ПК (i386)"

#: ../../english/releases/arches.data:19
msgid "kFreeBSD 64-bit PC (amd64)"
msgstr "kFreeBSD 64-битный ПК (amd64)"

#: ../../english/releases/arches.data:20
msgid "Motorola 680x0"
msgstr "Motorola 680x0"

#: ../../english/releases/arches.data:21
msgid "MIPS (big endian)"
msgstr "MIPS (с порядком байтов от старшего к младшему)"

#: ../../english/releases/arches.data:22
msgid "64-bit MIPS (little endian)"
msgstr "64-битный MIPS (с порядком байтов от младшего к старшему)"

#: ../../english/releases/arches.data:23
msgid "MIPS (little endian)"
msgstr "MIPS (с порядком байтов от младшего к старшему)"

#: ../../english/releases/arches.data:24
msgid "PowerPC"
msgstr "PowerPC"

#: ../../english/releases/arches.data:25
msgid "POWER Processors"
msgstr "Процессоры POWER"

#: ../../english/releases/arches.data:26
msgid "IBM S/390"
msgstr "IBM S/390"

#: ../../english/releases/arches.data:27
msgid "IBM System z"
msgstr "IBM System z"

#: ../../english/releases/arches.data:28
msgid "SPARC"
msgstr "SPARC"

#~ msgid "Hurd (i386)"
#~ msgstr "Hurd (i386)"

#~ msgid "kFreeBSD (Intel x86)"
#~ msgstr "kFreeBSD (Intel x86)"

#~ msgid "kFreeBSD (AMD64)"
#~ msgstr "kFreeBSD (AMD64)"

#~ msgid "Intel IA-64"
#~ msgstr "Intel IA-64"

#~ msgid "HP PA/RISC"
#~ msgstr "HP PA/RISC"

#~ msgid "AMD64"
#~ msgstr "AMD64"

#~ msgid "Intel x86"
#~ msgstr "Intel x86"
